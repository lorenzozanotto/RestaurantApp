//
//  RestaurantTableViewCell.swift
//  FoodPin
//
//  Created by Lorenzo Zanotto on 09/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var cityLabel : UILabel!
    @IBOutlet var voteLabel : UILabel!
    @IBOutlet var thumbnailImageView : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
