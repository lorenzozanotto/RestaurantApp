//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by Lorenzo Zanotto on 09/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var restaurantNames = ["The Grand Montgomery", "Roast to Coast", "Barnes & Lorenzo", "Lyonard Bistro", "Friesco"]
    var restaurantLocations = ["Montreal, Dandiq", "Milan, Italy", "Paris, France", "London, UK", "Rome, Italy"]
    var restaurantRating = ["★★★★", "★★★", "★★★★", "★★★★", "★★★"]
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantNames.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RestaurantTableViewCell
        
        cell.nameLabel.text = restaurantNames[indexPath.row]
        cell.cityLabel.text = restaurantLocations[indexPath.row]
        cell.voteLabel.text = restaurantRating[indexPath.row]
        
        
        return cell;

}

}